namespace Blog_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.BaiViet");
            DropForeignKey("dbo.Tag_Post", "TagsID", "dbo.Tags");
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropIndex("dbo.Tag_Post", new[] { "TagsID" });
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.BaiViet", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Post_ID);
            
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
            DropTable("dbo.Tag_Post");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagsID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagsID });
            
            DropIndex("dbo.TagPosts", new[] { "Post_ID" });
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropForeignKey("dbo.TagPosts", "Post_ID", "dbo.BaiViet");
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
            DropTable("dbo.TagPosts");
            CreateIndex("dbo.Tag_Post", "TagsID");
            CreateIndex("dbo.Tag_Post", "PostID");
            AddForeignKey("dbo.Tag_Post", "TagsID", "dbo.Tags", "TagID", cascadeDelete: true);
            AddForeignKey("dbo.Tag_Post", "PostID", "dbo.BaiViet", "ID", cascadeDelete: true);
        }
    }
}
