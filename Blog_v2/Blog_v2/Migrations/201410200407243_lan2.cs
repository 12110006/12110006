namespace Blog_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagsID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagsID })
                .ForeignKey("dbo.Post", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagsID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagsID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_Post", new[] { "TagsID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropForeignKey("dbo.Tag_Post", "TagsID", "dbo.Tags");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Post");
            DropTable("dbo.Tag_Post");
            DropTable("dbo.Tags");
        }
    }
}
