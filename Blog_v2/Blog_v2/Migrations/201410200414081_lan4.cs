namespace Blog_v2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Post", newName: "BaiViet");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.BaiViet", newName: "Post");
        }
    }
}
