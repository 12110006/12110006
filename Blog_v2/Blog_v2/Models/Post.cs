﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_v2.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [Range(2, 100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]
        public string Title { set; get; }
        [StringLength(250,ErrorMessage="So luong ki tu qua nhieu 10 - 250",MinimumLength=10)]
        public string Body { set; get; }
        [DataType(DataType.EmailAddress)]
        public DateTime DataCreated { set; get; }
        public DateTime DateUpdate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
    }
}