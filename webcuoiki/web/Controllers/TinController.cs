﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class TinController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Tin/

        public ActionResult Index()
        {
            var tin = db.Tin.Include(t => t.User).Include(t => t.LoaiTin);
            return View(tin.ToList());
        }

        //
        // GET: /Tin/Details/5

        public ActionResult Details(int id = 0)
        {
            Tin tin = db.Tin.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            return View(tin);
        }

        //
        // GET: /Tin/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.User, "UserId", "UserName");
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten");
            return View();
        }

        //
        // POST: /Tin/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tin tin)
        {
            if (ModelState.IsValid)
            {
                db.Tin.Add(tin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.User, "UserId", "UserName", tin.UserId);
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", tin.LoaiTinId);
            return View(tin);
        }

        //
        // GET: /Tin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tin tin = db.Tin.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.User, "UserId", "UserName", tin.UserId);
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", tin.LoaiTinId);
            return View(tin);
        }

        //
        // POST: /Tin/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tin tin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.User, "UserId", "UserName", tin.UserId);
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", tin.LoaiTinId);
            return View(tin);
        }

        //
        // GET: /Tin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tin tin = db.Tin.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            return View(tin);
        }

        //
        // POST: /Tin/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tin tin = db.Tin.Find(id);
            db.Tin.Remove(tin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}