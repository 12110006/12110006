﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class QuangCaoController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /QuangCao/

        public ActionResult Index()
        {
            var quangcao = db.QuangCao.Include(q => q.LoaiTin);
            return View(quangcao.ToList());
        }

        //
        // GET: /QuangCao/Details/5

        public ActionResult Details(int id = 0)
        {
            QuangCao quangcao = db.QuangCao.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            return View(quangcao);
        }

        //
        // GET: /QuangCao/Create

        public ActionResult Create()
        {
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten");
            return View();
        }

        //
        // POST: /QuangCao/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuangCao quangcao)
        {
            if (ModelState.IsValid)
            {
                db.QuangCao.Add(quangcao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", quangcao.LoaiTinId);
            return View(quangcao);
        }

        //
        // GET: /QuangCao/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuangCao quangcao = db.QuangCao.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", quangcao.LoaiTinId);
            return View(quangcao);
        }

        //
        // POST: /QuangCao/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuangCao quangcao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quangcao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LoaiTinId = new SelectList(db.LoaiTin, "LoaiTinId", "ten", quangcao.LoaiTinId);
            return View(quangcao);
        }

        //
        // GET: /QuangCao/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuangCao quangcao = db.QuangCao.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            return View(quangcao);
        }

        //
        // POST: /QuangCao/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuangCao quangcao = db.QuangCao.Find(id);
            db.QuangCao.Remove(quangcao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}