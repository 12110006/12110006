﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class Rating
    {
        public int RatingId { set; get; }
        public string MoTa { set; get; }
        public int SoLanChon { set; get; }

        public int TinId { set; get; }
        public virtual Tin Tin { set; get; }
    }
}