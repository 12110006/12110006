﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class QuangCao
    {
        public int QuangCaoId { set; get; }
        public string MoTa { set; get; }
        public string Url { set; get; }
        public string UrlHinh { set; get; }

        public int LoaiTinId { set; get; }
        public virtual LoaiTin LoaiTin { set; get; }
    }
}