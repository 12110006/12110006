﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class Comment
    {
        public int CommentId { set; get; }
        public DateTime Ngay { set; get; }
        public string NoiDung { set; get; }
        public string HoTen { set; get; }

        public int TinId { set; get; }
        public virtual Tin Tin { set; get; }
    }
}