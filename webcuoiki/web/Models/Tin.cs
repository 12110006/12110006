﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class Tin
    {
        public int TinId { set; get; }
        public string TieuDe { set; get; }
        public string MoTa { set; get; }
        public DateTime NgayDang { set; get; }
        public string NoiDung { set; get; }
        public int SoLanXem { set; get; }
        public Boolean AnHien { set; get; }

        public int UserId { set; get; }
        public int LoaiTinId { set; get; }
        public virtual User User { set; get; }
        public virtual LoaiTin LoaiTin { set; get; }
        public virtual ICollection<Rating> Rating { set; get; }
        public virtual ICollection<Comment> Comment { set; get; }
    }
}