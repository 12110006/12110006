﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        [DataType(DataType.Password)]
        public string password { get; set; }
        
        public Boolean GioiTinh { set; get; }
        public string NgheNghiep { get; set; }
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string SoDT { set; get; }
        public virtual ICollection<Tin> Tins { set; get; }
    }
}