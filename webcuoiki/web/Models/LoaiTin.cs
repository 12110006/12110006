﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class LoaiTin
    {
        public int LoaiTinId { set; get; }
        public string ten { set; get; }
        public Boolean An { set; get; }
        public virtual ICollection<Tin> Tins { set; get; }
        public virtual ICollection<QuangCao> QuangCaos { set; get; }
    }
}