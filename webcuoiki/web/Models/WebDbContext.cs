﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class WebDbContext:DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Tin> Tin { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<LoaiTin> LoaiTin { get; set; }
        public DbSet<Rating> Rating { get; set; }
        public DbSet<QuangCao> QuangCao { get; set; }
    }
}