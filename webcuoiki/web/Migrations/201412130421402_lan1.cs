namespace web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        password = c.String(),
                        GioiTinh = c.Boolean(nullable: false),
                        NgheNghiep = c.String(),
                        email = c.String(),
                        SoDT = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Tins",
                c => new
                    {
                        TinId = c.Int(nullable: false, identity: true),
                        TieuDe = c.String(),
                        MoTa = c.String(),
                        NgayDang = c.DateTime(nullable: false),
                        NoiDung = c.String(),
                        SoLanXem = c.Int(nullable: false),
                        AnHien = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        LoaiTinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TinId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.LoaiTins", t => t.LoaiTinId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.LoaiTinId);
            
            CreateTable(
                "dbo.LoaiTins",
                c => new
                    {
                        LoaiTinId = c.Int(nullable: false, identity: true),
                        ten = c.String(),
                        An = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.LoaiTinId);
            
            CreateTable(
                "dbo.QuangCaos",
                c => new
                    {
                        QuangCaoId = c.Int(nullable: false, identity: true),
                        MoTa = c.String(),
                        Url = c.String(),
                        UrlHinh = c.String(),
                        LoaiTinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuangCaoId)
                .ForeignKey("dbo.LoaiTins", t => t.LoaiTinId, cascadeDelete: true)
                .Index(t => t.LoaiTinId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        RatingId = c.Int(nullable: false, identity: true),
                        MoTa = c.String(),
                        SoLanChon = c.Int(nullable: false),
                        TinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RatingId)
                .ForeignKey("dbo.Tins", t => t.TinId, cascadeDelete: true)
                .Index(t => t.TinId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Ngay = c.DateTime(nullable: false),
                        NoiDung = c.String(),
                        HoTen = c.String(),
                        TinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Tins", t => t.TinId, cascadeDelete: true)
                .Index(t => t.TinId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "TinId" });
            DropIndex("dbo.Ratings", new[] { "TinId" });
            DropIndex("dbo.QuangCaos", new[] { "LoaiTinId" });
            DropIndex("dbo.Tins", new[] { "LoaiTinId" });
            DropIndex("dbo.Tins", new[] { "UserId" });
            DropForeignKey("dbo.Comments", "TinId", "dbo.Tins");
            DropForeignKey("dbo.Ratings", "TinId", "dbo.Tins");
            DropForeignKey("dbo.QuangCaos", "LoaiTinId", "dbo.LoaiTins");
            DropForeignKey("dbo.Tins", "LoaiTinId", "dbo.LoaiTins");
            DropForeignKey("dbo.Tins", "UserId", "dbo.Users");
            DropTable("dbo.Comments");
            DropTable("dbo.Ratings");
            DropTable("dbo.QuangCaos");
            DropTable("dbo.LoaiTins");
            DropTable("dbo.Tins");
            DropTable("dbo.Users");
        }
    }
}
